import './App.css';
import Section from './components/Section';

function App() {

  return (
    <>
    <h1>Hello World!</h1>
    <Section 
      title="Academics" 
      image="https://www.cwu.edu/cwu-together/sites/cts.cwu.edu.fallguide2020/files/fpo-student-life.jpg" 
      text="Fall quarter instruction will look much different than it did over the past 16 months, with far more in-person course offerings to choose from. CWU also wants to encourage students to take advantage of the many on-campus resources available to you. Learn more about Central's academic outlook for this fall."
      linkUrl="https://cwu.edu/news"
      linkText="CWU News"
      bgColor="#cecece"
      secId="academics"
      alignment="left"
    />
    <Section 
      title="Student Life" 
      image="https://www.cwu.edu/sites/default/files/images/1Rugby.jpg"
      text="
      Fall quarter 2021 is almost here, and everyone in the CWU community is ready for a fresh start. After a year of physical distancing and virtual classes, we’re all looking forward to a more traditional campus experience than we’ve had in more than a year. 

      As more people get vaccinated for COVID-19, we can continue looking toward a future that includes more in-person classes and activities. But we must all do our part if we are going to achieve our goal of providing a safe environment for CWU students and employees. That means getting vaccinated as soon as possible.         "
      linkUrl="https://cwu.edu/"
      linkText="CWU Homepage"
      bgColor="#fff"
      secId="advising"
      alignment="right"
    />
    <Section 
      title="Community Wi-Fi" 
      image=""
      text="CWU provides free Wi-Fi across the campus. Additionally, the coverage area has been expanded through a partnership with three local government offices and a nonprofit to provide free internet access through a program called Community Wi-Fi.  CWU’s Wi-Fi hotspot, in the paved parking lot outside of Tomlinson Stadium is available to the public 24 hours a day, seven days a week."
      linkUrl="https://cwu.edu/web-services"
      linkText="CWU Web Services"
      bgColor="#cecece"
      secId="community"
      alignment="left"
    />
    </>
  );
}

export default App;
