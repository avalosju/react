import React from 'react';
import Styles from './SectionStyles';
import '../styles/Section.css';

const Section = ({ title, image, text, linkUrl, linkText, bgColor, secId, alignment }) => {

    return (
        <div className={`section-container-${secId} alignment-${alignment}`} style={{ backgroundColor: bgColor }}>
            <div className="grid-container">
                <div className="section-content grid-x grid-margin-x">
                    <div className={ alignment === "left" ? "medium-6 medium-offset-6" : "medium-6"}>
                        <h2><strong>{ title }</strong></h2>
                        <p>{ text }</p>
                        <a href={` ${linkUrl} `}>{ linkText }</a>
                    </div>
                </div>
            </div>
            < Styles image={ image } secId={ secId } alignment={ alignment } />
        </div>
    )
}


export default Section
