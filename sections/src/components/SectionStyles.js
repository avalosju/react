import React from 'react'

const SectionStyles  = ({ image, secId }) => {
    return (
        <>
            <style>   
                {`
                    .section-container-${secId}.alignment-left::before {
                        content: "";
                        position: absolute;
                        left: 0;
                        top: 0;
                        height: 100%;
                        width: 45%;
                        background-image: url("${ image }");
                        background-size: cover;
                        background-repeat: no-repeat;
                        background-position: top center;
                        clip-path: polygon(100% 50%, calc(100% - 100px) 100%, 0% 100%, 0% 0%, calc(100% - 100px) 0%);
                    }
                    .section-container-${secId} {
                        position: relative;
                        padding: 5rem 1rem;
                    }
                    .section-container-${secId}.alignment-right::after {
                        content: "";
                        position: absolute;
                        right: 0;
                        top: 0;
                        height: 100%;
                        width: 45%;
                        background-image: url("${ image }");
                        background-size: cover;
                        background-repeat: no-repeat;
                        background-position: top center;
                        clip-path: polygon(0% 50%, 200px 0%, 1px 0%, 100% 0%, 100% 100%, 1px 100%, 200px 100%);
                    }

                    
                    @media screen and (max-width: 39.9375em) {
                        .section-container-${secId}.alignment-left::before, .section-container-${secId}.alignment-right::before {
                            content: "";
                            position: relative;
                            clip-path: none;
                            height: 18rem;
                            display: block;
                            width: 100%;
                            background-image: url("${ image }");
                            background-size: cover;
                            background-repeat: no-repeat;
                            background-position: top center;
                        }
                        .section-container-${secId}.alignment-right::after {
                            content: "";
                            display: none;
                        }
                        .section-container-${secId} {
                            padding: 1rem;
                        }
                    }
                `}
            </style>
        </>
    )
}

export default SectionStyles; 